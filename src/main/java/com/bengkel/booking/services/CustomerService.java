package com.bengkel.booking.services;

import java.util.List;

import com.bengkel.booking.models.Customer;

public class CustomerService {
    public static Customer getCustomerById(List<Customer> allCustomer, String customerId){
        return allCustomer.stream()
                .filter(customer -> customer.getCustomerId().equals(customerId))
                .findFirst()
                .orElse(null);
    }
}
