package com.bengkel.booking.services;

import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.Vehicle;

public class VehicleService {
    
    public static String getVehicleTypeFromVehicleId(Customer customer, String vehicleId){
        return customer.getVehicles().stream()
                .filter(vehicle -> vehicle.getVehiclesId().equalsIgnoreCase(vehicleId))
                .map(Vehicle::getVehicleType)
                .findFirst()
                .orElse("");
    }
}
