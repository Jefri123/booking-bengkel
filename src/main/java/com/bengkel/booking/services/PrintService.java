package com.bengkel.booking.services;

import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.text.DecimalFormat;

import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class PrintService {
	private static Scanner input = new Scanner(System.in);
	
	public static void printMenu(String[] listMenu, String title) {
		
		System.out.print("\033[H\033[2J");  
    	System.out.flush(); 

		String line = "+---------------------------------+";
		int number = 1;
		String formatTable = " %-2s. %-25s %n";
		
		System.out.printf("%-25s %n", " ".repeat(10) + title);
		System.out.println(line);
		
		for (String data : listMenu) {
			if (number < listMenu.length) {
				System.out.printf(formatTable, number, data);
			}else {
				System.out.printf(formatTable, 0, data);
			}
			number++;
		}
		System.out.println(line);
		System.out.println();
	}
	
	public static void printVechicle(List<Vehicle> listVehicle) {
		String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-15s | %-5s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun", "Tipe Kendaraan");
	    System.out.format(line);
	    int number = 1;
	    String vehicleType = "";
	    for (Vehicle vehicle : listVehicle) {
	    	if (vehicle instanceof Car) {
				vehicleType = "Mobil";
			}else {
				vehicleType = "Motor";
			}
	    	System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getBrand(), vehicle.getTransmisionType(), vehicle.getYearRelease(), vehicleType);
	    	number++;
	    }
	    System.out.printf(line);
	}
	//Silahkan Tambahkan function print sesuai dengan kebutuhan.

	public static void printCustomerInformation(){
		String memberType = (BengkelService.getCustomer() instanceof MemberCustomer)? "Member" : "Non Member";
		System.out.println("\n" + memberType);

		System.out.println(" ".repeat(20) + "Customer Profile\n");
		System.out.println("Customer Id\t: " + BengkelService.getCustomer().getCustomerId());
		System.out.println("Nama\t\t: " + BengkelService.getCustomer().getName());
		System.out.println("Customer Status\t: " + memberType);
		System.out.println("Alamat\t\t: " + BengkelService.getCustomer().getAddress());

		if (BengkelService.getCustomer() instanceof MemberCustomer){
			System.out.println("Saldo Koin\t: " + (new DecimalFormat("Rp #,##0.00")).format(((MemberCustomer) BengkelService.getCustomer()).getSaldoCoin()));
		}
		System.out.println("\nList Kendaraan");
		PrintService.printVechicle(BengkelService.getCustomer().getVehicles());

		input.nextLine();
	}

	public static void printItemService(String vehicleType){
		String formatTable = "| %-2s | %-15s | %-20s | %-15s | %-15s |%n";
		String line = "+----+-----------------+----------------------+-----------------+-----------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Service Id", "Nama Service", "Tipe Kendaraan", "Harga");
	    System.out.format(line);
	    AtomicInteger number = new AtomicInteger(1);

	    
		ItemServiceRepository.getAllItemService().stream()
				.filter(service -> service.getVehicleType().equalsIgnoreCase(vehicleType))
				.forEach(services -> {
					System.out.format(formatTable, number.getAndIncrement(), services.getServiceId(), services.getServiceName(), services.getVehicleType(), (new DecimalFormat("Rp #,##0.00")).format(services.getPrice()));
				});
		
		System.out.printf(line);
	}

	public static String getItemServiceName(List<ItemService> allItemService){
        String itemServiceName =  allItemService.stream()
									.map(ItemService::getServiceName)
									.collect(Collectors.joining(", "));
		
		return truncateText(itemServiceName, 20);
    }

	public static void printBookingOrderInformation(){
		String formatTable = "| %-2s | %-17s | %-13s | %-14s | %-13s | %-13s | %-20s |%n";
		String line = "+----+-------------------+---------------+----------------+---------------+---------------+----------------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Booking Id", "Nama Customer", "Payment Method", "Total Service", "Total Payment", "List Service");
	    System.out.format(line);
	    AtomicInteger number = new AtomicInteger(1);

	    
		BookingOrderService.getAllBookingOrders().stream()
				.filter(bookingOrder -> bookingOrder.getCustomer().getCustomerId().equalsIgnoreCase(BengkelService.getCustomer().getCustomerId()))
				.forEach(bookingOrder -> {
					System.out.format(formatTable, number.getAndIncrement(), bookingOrder.getBookingId(), bookingOrder.getCustomer().getName(), bookingOrder.getPaymentMethod(), (new DecimalFormat("Rp #,##0.00")).format(bookingOrder.getTotalServicePrice()), (new DecimalFormat("Rp #,##0.00")).format(bookingOrder.getTotalPayment()), getItemServiceName(bookingOrder.getServices()));
				});
		
		System.out.printf(line);
		input.nextLine();
	}

	public static String truncateText(String text, int maxLength) {
        if (text.length() <= maxLength) {
            return text;
        }
        return text.substring(0, maxLength - 3) + "...";
    }

	
}
