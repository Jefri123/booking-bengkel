package com.bengkel.booking.services;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class BengkelService {
	private static Scanner input = new Scanner(System.in);
	private static Customer customer;
	
	//Silahkan tambahkan fitur-fitur utama aplikasi disini
	public static Customer getCustomer(){
		return BengkelService.customer;
	}

	public static void setCustomer(Customer customer){
		BengkelService.customer = customer;
	}

	
	//Login
	public static void loginProcess(List<Customer> listAllCustomer){
		String[] listMenuLogin = {"Login", "Exit"};
		PrintService.printMenu(listMenuLogin, "Aplikasi Booking Bengkel");
		String optionInput = Validation.validasiInput("Masukan Pilihan : ", "Input hanya boleh 1 / 0", "^[01]$");
		
		if (optionInput.equalsIgnoreCase("1")){
			System.out.print("\033[H\033[2J");  
    		System.out.flush(); 
			System.out.println(" ".repeat(25) + "Login\n");

			String customerId = Validation.loginValidation(listAllCustomer);
			customer = CustomerService.getCustomerById(listAllCustomer, customerId);
		
		} else if(optionInput.equalsIgnoreCase("0")){
			System.exit(0);
		}
	}

	
	//Info Customer
	
	//Booking atau Reservation
	public static void bookingProcess(List<Customer> listAllCustomer, List<ItemService> listAllItemService){
		String vehicleId = Validation.vehicleIdValidation(customer);
		String vehicleType = VehicleService.getVehicleTypeFromVehicleId(customer, vehicleId);

		System.out.println("\nList Service yang Tersedia:");
		PrintService.printItemService(vehicleType);
		MenuService.exitOrContinue();

		List<ItemService> itemService = Validation.itemServiceIdValidation(ItemServiceRepository.getItemServiceByVehicleType(listAllItemService, vehicleType), customer);

		String paymentMethod = "cash";
		if (customer instanceof MemberCustomer){
			while (true) {
				paymentMethod = Validation.validasiInput("Silahkan pilih metode pembayaran (Saldo coin atau cash) : ", "input tidak sesuai, ex : saldo coin, cash", "^(?i)(saldo coin|cash)$");
				
				if (paymentMethod.equalsIgnoreCase("saldo coin")){
					BookingOrder bookingOrderTemp = BookingOrderService.createBookingOrderWithoutId(customer, itemService, paymentMethod, ItemServiceRepository.getTotalPriceFromItemServiceList(itemService));
					MemberCustomer memberTemp = (MemberCustomer) customer;
					
					if (bookingOrderTemp.getTotalPayment() <= memberTemp.getSaldoCoin()){
						memberTemp.setSaldoCoin(memberTemp.getSaldoCoin() - bookingOrderTemp.getTotalPayment());
						customer = memberTemp;
						break;
					
					} else{
						String actionsForTopUp = Validation.validasiInput("Saldo coin tidak mencukupi, apakah anda ingin Top Up (Y/T) : ", "input hanya menerima Y / T", "^[yYtT]$");

						if (actionsForTopUp.equalsIgnoreCase("y")){
							MenuService.mainMenu();
						}
					}

				} else{
					break;
				}
			}
		}

		BookingOrder bookingOrderTemp = BookingOrderService.addBookingOrders(customer, itemService, paymentMethod, ItemServiceRepository.getTotalPriceFromItemServiceList(itemService));

		System.out.println("\nBooking Berhasil!!!");
		System.out.println("Total harga Service\t: " + (new DecimalFormat("Rp #,##0.00")).format(ItemServiceRepository.getTotalPriceFromItemServiceList(itemService)));
		System.out.println("Total Pembayaran\t: " + (new DecimalFormat("Rp #,##0.00")).format(bookingOrderTemp.getTotalPayment()));

		input.nextLine();
	}
	
	//Top Up Saldo Coin Untuk Member Customer
	public static void topUpProcess(){
		if (customer instanceof MemberCustomer){
			System.out.println(" ".repeat(25) + "Top Up Saldo Coin\n");
			String topUpSaldo = Validation.validasiInput("Masukan besaran Top Up: ", "Input hanya boleh bilangan bulat yang lebih besar dari 0", "^[1-9]\\d*$");

			MemberCustomer memberCustomerTemp = (MemberCustomer) customer;
			memberCustomerTemp.setSaldoCoin(memberCustomerTemp.getSaldoCoin() + Double.parseDouble(topUpSaldo));

			customer = memberCustomerTemp;

			System.out.println("Top Up berhasil");

		} else{
			System.out.println("Maaf fitur ini hanya untuk Member saja!");

		}

		input.nextLine();
	}
	
	//Logout

	public static boolean logoutProcess(){
		System.out.println("Logout");
		input.nextLine();
		return false;
	}
	
}
