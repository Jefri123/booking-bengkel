package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class Validation {
	private static Scanner input = new Scanner(System.in);
	
	public static String validasiInput(String question, String errorMessage, String regex) {
	    String result;
	    boolean isLooping = true;
	    do {
	      System.out.print(question);
	      result = input.nextLine();

	      //validasi menggunakan matches
	      if (result.matches(regex)) {
	        isLooping = false;
	      }else {
	        System.out.println(errorMessage);
	      }

	    } while (isLooping);

	    return result;
	  }
	
	public static int validasiNumberWithRange(String question, String errorMessage, String regex, int max, int min) {
	    int result;
	    boolean isLooping = true;
	    do {
	      result = Integer.valueOf(validasiInput(question, errorMessage, regex));
	      if (result >= min && result <= max) {
	        isLooping = false;
	      }else {
	        System.out.println("Pilihan angka " + min + " s.d " + max);
	      }
	    } while (isLooping);

	    return result;
	  }

	  public static String loginValidation(List<Customer> allCustomer){
		int countFailedLogin = 0;
		int MAX_ATTEMPTS = 3;

		while (countFailedLogin < MAX_ATTEMPTS){
			System.out.print("Masukan Customer Id : ");
			String customerId = input.nextLine();

			Customer customer = CustomerService.getCustomerById(allCustomer, customerId);
			if (customer != null){
				while (countFailedLogin < MAX_ATTEMPTS) {
					System.out.print("Masukan Password : ");
					String password = input.nextLine();

					if (customer.getPassword().equals(password)){
						return customerId;

					} else {
						System.out.println("Password yang anda Masukan Salah!");
						countFailedLogin++;
					}
				}

			} else {
				System.out.println("Customer Id Tidak Ditemukan atau Salah!");
				countFailedLogin++;
			}
		}
		System.exit(0);
		return "";
	  }

	  public static boolean isVehicleInCustomer(Customer customer, String vehicleId){
		return customer.getVehicles().stream()
					.anyMatch(vehicle -> vehicle.getVehiclesId().equalsIgnoreCase(vehicleId));
	  }

	  public static String vehicleIdValidation(Customer customer){
		while (true) {
			System.out.println("Masukan Vehicle Id :");
			String vehicleId = input.nextLine();

			if (isVehicleInCustomer(customer, vehicleId)){
				return vehicleId;
			}
			System.out.println("Notifikasi Pesan bahwa Kendaraan Tidak ditemukan.");
		}
	  }

	  public static boolean isServiceIdInItemService(List<ItemService> allItemServices, String serviceId){
		return allItemServices.stream()
					.anyMatch(itermService -> itermService.getServiceId().equalsIgnoreCase(serviceId));
	  }

	  public static boolean isItemServiceAlreadyInputed(List<ItemService> allItemServiceTemp, String serviceId){
		return allItemServiceTemp.stream()
					.anyMatch(itermService -> itermService.getServiceId().equalsIgnoreCase(serviceId));
	  }

	  public static List<ItemService> itemServiceIdValidation(List<ItemService> allItemServices, Customer customer){
		List<ItemService> listItemServiceInputTemp = new ArrayList<>();

		while (listItemServiceInputTemp.size() < customer.getMaxNumberOfService()) {
			System.out.print("Silahkan masukan Service Id: ");
			String serviceId = input.nextLine();

			ItemService itemServiceTemp = ItemServiceRepository.getItemServiceByServiceId(allItemServices, serviceId);

			if (itemServiceTemp != null){
				if (isServiceIdInItemService(allItemServices, serviceId)){
					if (!(isItemServiceAlreadyInputed(listItemServiceInputTemp, serviceId))){
						listItemServiceInputTemp.add(itemServiceTemp);
						
						if (listItemServiceInputTemp.size() < customer.getMaxNumberOfService()){
							String isAnotherItemService = Validation.validasiInput("Apakah anda ingin menambahkan Service Lainnya? (Y/T) : ", "Input tidak sesuai", "^(y|Y|t|T)$");
		
							if (isAnotherItemService.equalsIgnoreCase("t")){
								return listItemServiceInputTemp;
							}
						}
	
					}else{
						System.out.println("Service id sudah diinputkan");
					}
	
				} else{
					System.out.println("Service Id tidak ditemukan");
				}

			}	else {
					System.out.println("Service Id tidak ditemukan");
			}
		}

		return listItemServiceInputTemp;
	  }
}
