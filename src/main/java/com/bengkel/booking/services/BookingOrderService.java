package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;

public class BookingOrderService {
    public static List<BookingOrder> allBookingOrders = new ArrayList<>();

    public static List<BookingOrder> getAllBookingOrders(){
        return BookingOrderService.allBookingOrders;
    }

     public static String findHighestCategory(String category) {
        Map<String, Integer> highestIds = new HashMap<>();
        
        for (
            String entry : BookingOrderService.allBookingOrders.stream()
                                .map(BookingOrder::getBookingId)
                                .toList()
        ) {
            String[] parts = entry.split("-");
            
            String entryCategory = parts[0] + "-" + parts[1] + "-" + parts[2];
            int id = Integer.parseInt(parts[3]);
            
            highestIds.put(entryCategory, Math.max(highestIds.getOrDefault(entryCategory, 0), id));
        }
        
        int highestId = highestIds.getOrDefault(category, 0);
        return category + "-" + String.format("%03d", highestId);
    }

    public static String generateId(Customer customer){
        String id = findHighestCategory("Book-" + customer.getCustomerId());
        String[] listId = id.split("-");
        return String.format("Book-"+ customer.getCustomerId() +"-%03d", Integer.parseInt(listId[3]) + 1);
    }

    public static BookingOrder addBookingOrders(Customer customer, List<ItemService> services, String paymentMethod, double totalServicePrice){
        BookingOrder bookingOrderTemp = BookingOrder.builder()
                            .bookingId(generateId(customer))
                            .customer(customer)
                            .services(services)
                            .paymentMethod(paymentMethod)
                            .totalServicePrice(totalServicePrice)
                            .build();
        
        bookingOrderTemp.calculatePayment();

        BookingOrderService.allBookingOrders.add(bookingOrderTemp);

        return bookingOrderTemp;
    }

    public static BookingOrder createBookingOrderWithoutId(Customer customer, List<ItemService> services, String paymentMethod, double totalServicePrice){
        BookingOrder bookingOrderTemp = BookingOrder.builder()
                            .customer(customer)
                            .services(services)
                            .paymentMethod(paymentMethod)
                            .totalServicePrice(totalServicePrice)
                            .build();
        
        bookingOrderTemp.calculatePayment();

        return bookingOrderTemp;
    }


}
