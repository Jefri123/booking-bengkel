package com.bengkel.booking.services;

import java.util.List;

import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
	private static List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
	private static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();

	public static void run() {
		boolean isLooping = true;
		do {
			login();
			mainMenu();
		} while (isLooping);
		
	}
	
	public static void login() {
		BengkelService.loginProcess(listAllCustomers);
	}
	
	public static void mainMenu() {
		String[] listMenu = {"Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking", "Logout"};
		int menuChoice = 0;
		boolean isLooping = true;
		
		do {
			System.out.print("\033[H\033[2J");  
    		System.out.flush();
			PrintService.printMenu(listMenu, "Booking Bengkel Menu");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu : ", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length-1, 0);	
			switch (menuChoice) {
			case 1:
				//panggil fitur Informasi Customer
				PrintService.printCustomerInformation();
				break;
			case 2:
				//panggil fitur Booking Bengkel
				BengkelService.bookingProcess(listAllCustomers, listAllItemService);
				break;
			case 3:
				//panggil fitur Top Up Saldo Coin
				BengkelService.topUpProcess();
				break;
			case 4:
				//panggil fitur Informasi Booking Order
				PrintService.printBookingOrderInformation();
				break;
			default:
				isLooping = BengkelService.logoutProcess();
				break;
			}
		} while (isLooping);
		
		
	}
	
	//Silahkan tambahkan kodingan untuk keperluan Menu Aplikasi
	public static void exitOrContinue(){
        String inp = Validation.validasiInput("\nTekan Enter untuk Continue atau ketik Exit untuk kembali ke main menu ! : ",  "Input tidak boleh selain exit !", "^(|(?i)Exit)$");
        if (inp.equalsIgnoreCase("exit")){
            MenuService.mainMenu();
        }
    }
}
